function createCard(name, location, description, pictureUrl, starts, ends) {
    function formateDate(date){
        const year = date.slice(0, 4);
        const month = date.slice(5,7);
        const day = date.slice(8, 10);
        return `${month}/${day}/${year}`
    }
    return `
      <div class="card shadow p-3 mb-5 bg-body rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-subtitle mb-2 text-muted">${location}</p>
          <p class="card-text">${description}</p>
        <div class="card-footer"><p>${formateDate(starts)} - ${formateDate(ends)}</p></div>
        </div>
      </div>
    `;
  }



  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
        let counter = 0;
        for (let conference of data.conferences) {
            counter += 1;
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const location = details.conference.location.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = details.conference.starts
            const ends = details.conference.ends
            const html = createCard(title, location, description, pictureUrl, starts, ends);
            const column1 = document.querySelector('.col1');
            const column2 = document.querySelector('.col2');
            const column3 = document.querySelector('.col3');
            if (counter%3==1){
                column1.innerHTML += html;
            } else if (counter%3==2){
                column2.innerHTML += html;
            } else {
                column3.innerHTML += html;
            }
          }
        }

      }
    } catch (e) {
      console.error(e);
    }

  });
